package main
// will use the bible.helloao.org to begin
// but I am counting on migrating to my own system one day
// TODO: migrate

import (
    "net/http"
    "encoding/json"
)

const STATIC_DIR = "./static"
const PORT = ":7000"

type Res struct {
    Name    string `json:"name"`
    Content string `json:"content"`
}

func main() {
    mux := http.NewServeMux()
    mux.Handle("/", http.FileServer(http.Dir(STATIC_DIR)))
    mux.HandleFunc("/{book}/{chapter}/{verse}", getPassage)

    http.ListenAndServe(PORT, mux)
}

func getPassage(w http.ResponseWriter, r *http.Request) {
    book    := r.PathValue("book")
    chapter := r.PathValue("chapter")
    verse   := r.PathValue("verse")

    data := Res {}
    data.Name = "The holy name"
    data.Content = "The wholesome holy content: " + book + chapter + verse
    
    w.Header().Set("Content-Type", "application/json")

    dataJson, err := json.Marshal(data)
    if err != nil {
        panic(err)
    }
    w.WriteHeader(http.StatusCreated)

    w.Write(dataJson)
}

