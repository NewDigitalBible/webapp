module Main exposing (main)

import Browser
import Html exposing (Html)

-- search css' equivalent attributes :
-- package.elm-lang.org/packages/rtfeldman/elm-css/latest/Css
import Css exposing (..)

import Html.Styled exposing (..)
import Html.Styled.Events exposing (onClick, onInput)
import Html.Styled.Attributes exposing (css, href, src, value)
import List exposing (member)


books =
    [ "Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua", "Judges", "Ruth", "Samuel", "Kings", "Chronicles", "Ezra", "Nehemiah", "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes", "Song of Solomon", "Isaiah", "Jeremiah", "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos", "Obadiah", "Jonah", "Micah", "Nahum", "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi", "Matthew", "Mark", "Luke", "Acts", "Romans", "Corinthians", "Galatians", "Ephesians", "Philippians", "Colossians", "Thessalonians", "Timothy", "Titus", "Philemon", "Hebrews", "James", "Peter", "John", "Jude", "Revelation" ]


main =
    Browser.sandbox
        { init = init
        , update = update
        , view = view
        }


--type Verse = SingleVerse Int | RangeVerse (Int, Int)


type alias Model =
    { name : String
    , content : String
    , book : String
    , chapter : String
    --  , verse : Verse
    }


init : Model
init =
    { name = ""
    , content = ""
    , book = "Genesis"
    , chapter = "1"
    --  , verse = SingleVerse 1
    }


type Msg
    = Fetch
    | UpdateBook String
    | UpdateChapter String
--  | UpdateVerse String

err model error =
    { model | name = "Error"
            , content = error 
    }

unrecognised book =
    "'" ++ book ++ "' has not been recognised as the valid name of a book"

nan str =
    "'" ++ str ++ "' is not a number"

fetchPassage : (String, Int) -> String -- TODO: add Verse
fetchPassage u = "Unimplemented"

getPassage model =
            let book = model.book in
            let chapter = model.chapter in
            if member book books then
                case String.toInt chapter of
                    Just x ->
                        let content = fetchPassage (book, x) in
                        { model | name = book
                                , content = content 
                        }
                    Nothing -> 
                        err model (nan chapter)

            else 
                err model (unrecognised book)




update msg model =
    case msg of
        Fetch ->
            getPassage model

        UpdateBook newBook ->
            { model | book = newBook }

        UpdateChapter newChapter ->
            { model | chapter = newChapter }

body model =
    div
        [ css
            [ alignSelf center
            ]
        ]
        [ h1 [css [textAlign center]] [ text "New Digital Bible" ]
        , input [ value model.book, onInput UpdateBook ] []
        , input [ value model.chapter, onInput UpdateChapter ] []
        --  , input [ value model.verse, onInput UpdateVerse ] []
        , div []
            [ text ("Book: " ++ model.book)
            , text (" Chapter: " ++ model.chapter)
            --      , text (" Verse: " ++ model.verse)
            ]
        , button [ onClick Fetch ] [ text "Fetch" ]
        , div []
            [ h2 [] [ text model.name ]
            , text model.content
            ]
        ]

view : Model -> Html.Html Msg
view model =
    Html.Styled.toUnstyled (
        div [css 
                [ displayFlex
                , justifyContent center
                ]
            ] 
            [body model])

